#!/bin/bash
DATABASE=$1
PROP=""
if [ "$2" == "SAT" ]
then
    PROP="conf/ts-sat.properties"
    DATABASE=$DATABASE"-sat"
else
    PROP="conf/ts.properties"
fi

java -Xmx100g -jar RDFQuotient-1.8-with-dependencies.jar --summarize "database.name=$DATABASE,dataset.filename=$1.nt" -sp $PROP
