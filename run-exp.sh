#!/bin/bash

DATE=`date +"%Y-%m-%d-%H-%M-%S"`
ANSWERS_DIR=/tmp
ISQL="loading/virtuoso-opensource/bin/isql 1111 dba dba"
TIMEOUT=600
NB_EXEC=5

ENGINE=$1
DB=$2
QTRANS=$3
DLAYOUT=$4
SUMMARY_TABLE=$5

if [ $ENGINE == "VIRTUOSO" ]
then
    SUMMARY_TABLE=$4
fi

RESULT_NAME=$DATE"_"$ENGINE"_"$DB"_"$QTRANS
# result file name
if [ -n "$DLAYOUT" ]
then
    RESULT_NAME=$RESULT_NAME"_"$DLAYOUT
fi

if [ -n "$SUMMARY_TABLE" ]
then
    RESULT_NAME=$RESULT_NAME"_PRUN"
fi

echo $SUMMARY_TABLE

mkdir -p results

run_ontosql() {
    PROP_FILE=run-exp.properties
    TMP_PROP_FILE=".tmp_"$DATE"_"$PROP_FILE

    #create the temporary properties file
    cp $PROP_FILE $TMP_PROP_FILE

    #set the timeout
    echo "querysession.query_timeout = $TIMEOUT" >> $TMP_PROP_FILE

    ARGS=" -pf $TMP_PROP_FILE -nbrExec $NB_EXEC -pSQL -exec"

    # database name (lubm100m or bench)
    if [ -z $DB ]
    then
        echo "second input is the database name"
        exit 1;
    else
        ARGS=$ARGS" -qr queries/${DB/%[0-9]*m/}.queries"
    fi

    # summary table 
    if [ -z $SUMMARY_TABLE ]
    then
        echo "no summary based pruning"
    else
        echo "pruning.summary_tablename=$SUMMARY_TABLE" >> $TMP_PROP_FILE
    fi
    
    #query translation (SAT, SAT_INS, REF_INS, REF)
    case $QTRANS in
        SAT)
            ARGS=$ARGS" -tt 3 -isSaturated"
            echo "reasoner.minimizing = true" >> $TMP_PROP_FILE
            DB=$DB"-sat"
            echo "saturation.type = RDFS_SAT" >> $TMP_PROP_FILE
            echo "reasoner.type = NONE" >> $TMP_PROP_FILE;;
        SAT_REF_RC_INS)
            ARGS=$ARGS" -tt 3 -isSaturated"
            echo "saturation.type = RDFS_SAT" >> $TMP_PROP_FILE
            echo "reasoner.minimizing = false" >> $TMP_PROP_FILE
            DB=$DB"-sat"
            echo "reasoner.type = CONSTRAINT_REF" >> $TMP_PROP_FILE;;
        SAT_REF_RC)
            ARGS=$ARGS" -tt 3 -isSaturated"
            echo "saturation.type = RDFS_SAT" >> $TMP_PROP_FILE
            echo "reasoner.minimizing = true" >> $TMP_PROP_FILE
            DB=$DB"-sat"
            echo "reasoner.type = CONSTRAINT_REF" >> $TMP_PROP_FILE;;
        SAT_INS)
            ARGS=$ARGS" -tt 3 -isSaturated"
            echo "saturation.type = RDFS_SAT" >> $TMP_PROP_FILE
            echo "reasoner.minimizing = false" >> $TMP_PROP_FILE
            DB=$DB"-sat"
            echo "reasoner.type = NONE" >> $TMP_PROP_FILE;;
        REF_INS)
            ARGS=$ARGS" -tt 3"
            echo "reasoner.minimizing = false" >> $TMP_PROP_FILE;;
        REF)
            ARGS=$ARGS" -tt 3"
            echo "reasoner.minimizing = true" >> $TMP_PROP_FILE;;
        *)
            echo "third input should define the query tranfomation among : SAT, SAT_INS, REF_INS, REF, SAT_REF_RC_INS, SAT_REF_RC"
            exit 1;;
    esac

    # set the database name
    echo "database.name = "$DB >> $TMP_PROP_FILE
    
    #data layout
    case $DLAYOUT in
        T)
            ARGS=$ARGS" -TTranslate";;
        CP)
        ;;
        CP_NAIVE)
            ARGS=$ARGS" -isDirect";;
        TCP)
            ARGS=$ARGS" -isHybrid";;
        *)
            echo "fourth input should define the data layout among : T, CP, CP_NAIVE, TCP"
            exit 1;;
    esac

    # set the result name
    ARGS=$ARGS" -resultF "$RESULT_NAME

    # set the directory for answers
    ARGS=$ARGS" -answersDir "$ANSWERS_DIR

    # restart postgres
    #sudo systemctl restart postgresql.service
    
    # run VACCUM
    psql -d $DB -c "VACUUM VERBOSE ANALYZE" &> /dev/null
    
    CMD="java -Xmx100G -jar QUERYEXEC_NEW-1.0.4-SNAPSHOT.jar$ARGS"
    echo $CMD
    $CMD && rm $TMP_PROP_FILE
    
}

run_virtuoso() {

    # result file name
    if [ -n "$SUMMARY_TABLE" ]
    then
        TMP_PROP_FILE=".tmp_"$DATE"_pruning.properties"
        cp summaries.properties $TMP_PROP_FILE
        echo "database.name = "$DB >> $TMP_PROP_FILE
        echo "pruning.summary_tablename=$SUMMARY_TABLE" >> $TMP_PROP_FILE
    fi

    # database name (lubm100m or bench)
    if [ -z $DB ]
    then
        echo "second input is the database name"
        exit 1;
    else
        GRAPH=$DB
        CQ=queries/${DB/%[0-9]*m/}.queries
        QUERY=${DB/%[0-9]*m/}
    fi

    INS="MIN"
    #query translation (SAT, SAT_INS, REF_INS, REF)
    case $QTRANS in
        SAT)
            QUERY=$QUERY.sparql
            GRAPH=$GRAPH"-sat";;
        SAT_INS)
            QUERY=$QUERY.sparql
            GRAPH=$GRAPH"-sat"
            INS="INS";;
        REF_INS)
            QUERY=$QUERY"_REF_INS.sparql"
            INS="INS";;
        REF)
            QUERY=$QUERY"_REF_MIN.sparql";;
        *)
            echo "third input should define the query tranfomation among : SAT, SAT_INS, REF_INS, REF"
            exit 1;;
    esac

    # build sparql queries files and save the useful file
    QUERY_PATH=results/$RESULT_NAME/$QUERY
    build_sparql_query $CQ "<$GRAPH>" loading/data/${DB/%[0-9]*m/}-ontology.nt $INS $TMP_PROP_FILE 
    mkdir -p results/$RESULT_NAME
    mv $QUERY $QUERY_PATH
    # # apply prefix
    # # remove sparql
    # # sed -i 's?SPARQL ??g' $QUERY_PATH
    # # sed -i 's?http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#?ub:?g' $QUERY_PATH
    # # sed -i 's?http://www.w3.org/2000/01/rdf-schema#?rdfs:?g' $QUERY_PATH
    # # sed -i 's?http://www.w3.org/1999/02/22-rdf-syntax-ns#?rdf:?g' $QUERY_PATH
    # rm *.sparql

    # evaluate queries
    STAT=results/$RESULT_NAME/$RESULT_NAME.csv

    for i in $(seq 0 1); do
        # end of cold run
        if [ $i -eq 1 ] ; then
            echo "query,EXEC_TIME,NANS" > $STAT
            NB_LOOP=$NB_EXEC
        else
            NB_LOOP=1
        fi

        OUT=results/$RESULT_NAME/
        PREFIXES="" #" sparql prefix ub: <http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#> prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
        QUERY_STRING=$PREFIXES
        QUERY_NAME=""
        QUERY_LIMIT=1500000
        QUERY_TMP_FILE=$ANSWERS_DIR"/virtuoso-query.sql"
        while IFS= read -r line; do
            
            if [ -z "$line" ]
            then
                ANSWERS_PATH=$ANSWERS_DIR/$QUERY_NAME.txt
                #put ; at the end of the query !!!
                echo ";" >> $QUERY_TMP_FILE
                for k in $(seq 1 $NB_LOOP); do
                    exec_sparql_virtuoso $QUERY_TMP_FILE > $ANSWERS_PATH
                    if [ "$?" -eq 124 ] ; then
                        echo "TIMEOUT"
                        ans="TIMEOUT"
                        ms="TIMEOUT"
                    else
                        echo "OK"
                        ans=`grep "Rows" $ANSWERS_PATH | sed -n 's/^\([0-9]*\).*/\1/p'`
                        ms=`grep "Rows" $ANSWERS_PATH | sed -n 's/.*-- \([0-9]*\).*/\1/p'`
                    fi

                    echo $QUERY_NAME","$ms","$ans >> $STAT

                    if [ -n "$ms" ] && [ $ms == "TIMEOUT" ] ; then
                        break
                    fi
                done
                mv  $ANSWERS_PATH $OUT$QUERY_NAME.txt

            elif [ ${line:0:1} == "#" ]
            then
                QUERY_NAME=${line:2:4}
                echo $QUERY_NAME
                #empty the query tmp file
                > $QUERY_TMP_FILE
            else
                # concat the query definition
                echo $line >> $QUERY_TMP_FILE
            fi
        done < $QUERY_PATH
        rm $QUERY_TMP_FILE
    done
 }

exec_sparql_virtuoso() {
    timeout $TIMEOUT $ISQL -b 10000  exec="LOAD $1"
}

build_sparql_query() {
    java -cp QUERYEXEC_NEW-1.0.4-SNAPSHOT.jar fr.inria.cedar.ontosql.querysession.utils.QueryExporter $1 $2 $3 $4 $5
}

run_rdfox() {
    RDF_BIN=./RDFox/RDFox
    #RESULT_NAME="2019-11-21-12-34-34_RDFOX_lubm100m_SAT"
    # database name (lubm100m or bench)
    if [ -z $DB ]
    then
        echo "second input is the database name"
        exit 1;
    else
        GRAPH=$DB
        LOADING_SCRIPT=loading/rdfox-${DB/%[0-9]*m/}.txt
        QUERIES_SCRIPT=queries/rdfox-${DB/%[0-9]*m/}.queries
        QUERY=${DB/%[0-9]*m/}.txt
    fi

    #query translation (SAT, SAT_INS, REF_INS, REF)
    case $QTRANS in
        SAT)
        ;;
        SAT_INS)
            echo "not supported yet"
            exit 1;;
        REF_INS)
            echo "not supported yet"
            exit 1;;

        REF)
            echo "not supported yet"
            exit 1;;
        *)
            echo "third input should define the query tranfomation among : SAT, SAT_INS, REF_INS, REF"
            exit 1;;
    esac

    # build the command file
    QUERY_PATH=results/$RESULT_NAME/$QUERY
    mkdir results/$RESULT_NAME
    cat $LOADING_SCRIPT > $QUERY_PATH

    for i in $(seq 0 $NB_EXEC); do
        cat $QUERIES_SCRIPT >> $QUERY_PATH
    done

    # evaluate queries
    LOG=results/$RESULT_NAME/$RESULT_NAME.log
    STAT=results/$RESULT_NAME/$RESULT_NAME.csv

    $RDFOX_BIN daemon . "exec $QUERY_PATH $DB results/$RESULT_NAME/" > $LOG

    echo "QUERY, EXEC_TIME, NANS" > $STAT
    query_name=""
    exec_time=""
    nb_ans=""
    while IFS= read -r line; do
        if [[ $line == "output"* ]]
        then
            query_name=`echo $line | sed -n 's/.*\(Q[0-9]*\)" (.*/\1/p'`
        fi
        if [[ $line == *"number"* ]]
        then
            nb_ans=`echo $line | sed -n 's/.* \([0-9]*\).*/\1/p'`
        fi

        if [[ $line == *"time"* ]]
        then
            exec_time_s=`echo $line | sed -n 's/.* \([0-9]*\.\{0,1\}[0-9]*\) s.*/\1/p'`
            exec_time=$(bc -l <<<"${exec_time_s}*1000")
            exec_time=`echo $exec_time | sed -n 's/^\([0-9]*\).*/\1/p'`
            echo $query_name","$exec_time","$nb_ans >> $STAT
        fi

    done < $LOG

}


LOG_FILE=results/$RESULT_NAME/log

mkdir -p results/$RESULT_NAME

case $ENGINE in
    ONTOSQL)
        run_ontosql
        ;;
    VIRTUOSO)
        run_virtuoso
        ;;
    RDFOX)
        run_rdfox
        ;;
    *)
        echo "first input is the query engine ONTOSQL, RDFOX or VIRTUOSO"
        exit 1
        ;;
esac 2>&1 | tee -a -i $LOG_FILE
