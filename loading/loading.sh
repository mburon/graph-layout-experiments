#!/bin/bash

# export and import directory for virtuoso
EXPORT_DIR=/tmp/
# command of the client connection for virtuoso 
ISQL="virtuoso-opensource/bin/isql 1111 dba dba"
# ontosql rdfdb jar file 
ONTOSQL_JAR="ontosql-rdfdb-1.0.11.jar"

mkdir -p log

DB=$2
if [ -z $DB ]
then
    echo "second input should be a data file name"
    exit 1
fi

DATA_FILE=data/$DB.nt
ONTO_FILE=data/${DB/%[0-9]*m/}-ontology.nt

PROP_FILE=loading.properties
DATE=`date +"%Y-%m-%d-%H-%M-%S"`
TMP_PROP_FILE=".tmp_"$DATE"_"$PROP_FILE

cp $PROP_FILE $TMP_PROP_FILE


if [ "$3" == "SAT" ]
then
    SAT_EXT="-sat"
    echo "database.name = "$DB"-sat" >> $TMP_PROP_FILE;
    echo "saturation.type = RDFS_SAT" >> $TMP_PROP_FILE;
else
    echo "database.name = "$DB >> $TMP_PROP_FILE;
    echo "saturation.type = NONE" >> $TMP_PROP_FILE;
fi

ENGINE=$1
LOG_FILE=log/$DB"_"$ENGINE$SAT_EXT.log

{
    date 
    echo "" 
    echo "----------" 
    echo ""


    case $ENGINE in
        ONTOSQL)
            ARGS=" -pf $TMP_PROP_FILE"
            if [ -z $ONTO_FILE ]
            then
                ARGS=$ARGS" -input $DATA_FILE"
            else
                ARGS=$ARGS" -data $DATA_FILE -rdfs $ONTO_FILE"
            fi

            CMD="java -Xmx100G -jar $ONTOSQL_JAR $ARGS"
            echo $CMD
            $CMD
            ;;
        VIRTUOSO)
            EXPORT_PATH=$DB$SAT_EXT.nt
            
            if [ -z $SAT_EXT ]
            then
                cp data/$DB.nt $EXPORT_DIR$EXPORT_PATH
                #EXPORT=/data/datasets/$DB/$DB.nt
            else
                # export of saturated graph in nt file
                > $EXPORT_DIR$EXPORT_PATH

                psql -t -d $DB$SAT_EXT -AF ' ' --no-align --single-transaction --set AUTOCOMMIT=off -c "select SD.value, PD.value, OD.value from (select distinct s, p, o from encoded_saturated_triples) AS T, dictionary AS SD, dictionary AS PD, dictionary AS OD WHERE SD.key=T.s AND PD.key=T.p AND OD.key=T.o" > $EXPORT_DIR$EXPORT_PATH
                sed -i 's/$/ \./' $EXPORT_DIR$EXPORT_PATH
            fi

            #copy the ontology
            cat data/${DB/%[0-9]*m/}-ontology.nt >> $EXPORT_DIR$EXPORT_PATH

            $ISQL exec="delete from DB.DBA.load_list;"
            $ISQL exec="sparql clear graph <$DB$SAT_EXT>;"
            COM="ld_dir('$EXPORT_DIR', '$EXPORT_PATH', '$DB$SAT_EXT');"
            echo $COM
            $ISQL exec="$COM"
            $ISQL exec="rdf_loader_run();"
            $ISQL exec="checkpoint;"
            rm $EXPORT_DIR$EXPORT_PATH
            ;;
        *)
            echo "first input should be a engine type among VIRTUOSO and ONTOSQL"
            exit 1;;
    esac

    echo ""
    echo "----------"
    echo ""

    date

} 2>&1 | tee -a -i $LOG_FILE

exit 0
